//Entry point script that finds all the package manager files in repo and passes it to the ruby script index.rb

require('dotenv').config();
const { findPackageManagerFiles } = require('../../utils/findPackageManagerFiles')
const { getProjectIdFromProjectPath } = require('../../utils/getProjectId');
const { spawn } = require('child_process');
const axios = require('axios');

if(!process.env.PROJECT_PATH)
  process.env.PROJECT_PATH = process.argv[2];

if(!process.env.GITLAB_ACCESS_TOKEN)
  process.env.GITLAB_ACCESS_TOKEN = process.argv[3];

if(!process.env.GITHUB_ACCESS_TOKEN)
  process.env.GITHUB_ACCESS_TOKEN = process.argv[4];

if(!process.env.TEAMS_URL)
  process.env.TEAMS_URL = process.argv[5];

const main = async () => {
  if (!process.env.PROJECT_PATH) {
    console.error('Please set the PROJECT_PATH in environment variables');
    return;
  }
  if (!process.env.GITLAB_ACCESS_TOKEN) {
    console.error('Please set the GITLAB_ACCESS_TOKEN in environment variables')
    return;
  }
  const projectId = await getProjectIdFromProjectPath(process.env.PROJECT_PATH, process.env.GITLAB_ACCESS_TOKEN);
  if (!projectId) {
    console.error('Failed to fetch gitlab apis');
    return;
  }

  try {
    const packageManagerFilesLocations = await findPackageManagerFiles(projectId, process.env.GITLAB_ACCESS_TOKEN);
    const depData = await getDependencies(packageManagerFilesLocations, projectId);
    const rubyScriptPath = './index.rb';
    const rubyProcess = spawn('ruby', [rubyScriptPath, JSON.stringify(packageManagerFilesLocations),JSON.stringify(depData)]);

    rubyProcess.stdout.on('data', (data) => {
      console.log(data.toString());
    });

    rubyProcess.stderr.on('data', (data) => {
      console.log(data.toString());
    });

    rubyProcess.on('close', (code) => {
      console.log(`Process exited with code ${code}`);
    });

  } catch (error) {
    console.error('Error:', error.message);
  }
};

//Function to fetch all the dependencies and their versions from a package manager file
const getDependencies = async (packageManagerFilesLocations, projectId) => {
  let dependenciesData = {};

  await Promise.all(packageManagerFilesLocations.map(async elem => {
    const url = `https://gitlab.com/api/v4/projects/${encodeURIComponent(projectId)}/repository/files/${encodeURIComponent(elem.substring(1) + (elem.length != 1 ? '/' : '') + 'package.json')}/raw`;
    const HEADERS = {
      'PRIVATE-TOKEN': process.env.GITLAB_ACCESS_TOKEN
    };
    const response = await axios.get(url, { headers: HEADERS });
    const fileContent = response.data;

    const dependenciesTypeList = ['dependencies', 'devDependencies', 'peerDependencies', 'optionalDependencies'];
    dependenciesTypeList.forEach(depType => {
      if (!fileContent[depType]) return;
      const depList = Object.keys(fileContent[depType]);
      depList.forEach(dep => {
        let version = fileContent[depType][dep];
        if (isNaN(version[0])) version = version.substring(1);
        if(!(dep in dependenciesData))
          dependenciesData[dep]=[]
        dependenciesData[dep].push({
          version: version,
          location: elem
        });
      });
    });
  }));

  return dependenciesData;
}

main();