# This script is designed to loop through all dependencies in a GHE, GitLab or
# Azure DevOps project, creating PRs where necessary.

require "dependabot/file_fetchers"
require "dependabot/file_parsers"
require "dependabot/update_checkers"
require "dependabot/file_updaters"
require "dependabot/pull_request_creator"
require "dependabot/omnibus"
require "gitlab"
require "json"


# Full name of the repo you want to create pull requests for.
repo_name = ENV["PROJECT_PATH"] # namespace/project

# Directory where the base dependency files are.
directory = ENV["DIRECTORY_PATH"] || "/"

# Branch to look at. Defaults to repo's default branch
branch = ENV["BRANCH"]

# Name of the package manager you'd like to do the update for. Options are:
# - bundler
# - pip (includes pipenv)
# - npm_and_yarn
# - maven
# - gradle
# - cargo
# - hex
# - composer
# - nuget
# - dep
# - go_modules
# - elm
# - submodules
# - docker
# - terraform
package_manager = "npm_and_yarn"

# Expected to be a JSON object passed to the underlying components
options = JSON.parse(ENV["OPTIONS"] || "{}", {:symbolize_names => true})
puts "Running with options: #{options}"


gitlab_hostname = ENV["GITLAB_HOSTNAME"] || "gitlab.com"

credentials = [
  {
    "type" => "git_source",
    "host" => "github.com",
    "username" => "x-access-token",
    "password" => ENV["GITHUB_ACCESS_TOKEN"] # A GitHub access token with read access to public repos
  },
  {
    "type" => "git_source",
    "host" => gitlab_hostname,
    "username" => "x-access-token",
    "password" => ENV["GITLAB_ACCESS_TOKEN"] # A GitLab access token with API permission
  }
]

source = Dependabot::Source.new(
  provider: "gitlab",
  hostname: gitlab_hostname,
  api_endpoint: "https://#{gitlab_hostname}/api/v4",
  repo: repo_name,
  directory: directory,
  branch: branch,
)


##############################
# Fetch the dependency files #
##############################
puts "Fetching #{package_manager} dependency files for #{repo_name}"
fetcher = Dependabot::FileFetchers.for_package_manager(package_manager).new(
  source: source,
  credentials: credentials,
  options: options,
)

files = fetcher.files
commit = fetcher.commit

##############################
# Parse the dependency files #
##############################
puts "Parsing dependencies information"
parser = Dependabot::FileParsers.for_package_manager(package_manager).new(
  dependency_files: files,
  source: source,
  credentials: credentials,
  options: options,
)

dependencies = parser.parse

dependencies.select(&:top_level?).each do |dep|
  next if dep.name && @ignoreDependenciesHash.has_key?(dep.name) && @ignoreDependenciesHash[dep.name]=="all" #New code

  #########################################
  # Get update details for the dependency #
  #########################################
  checker = Dependabot::UpdateCheckers.for_package_manager(package_manager).new(
    dependency: dep,
    dependency_files: files,
    credentials: credentials,
    options: options,
  )
  next if checker.up_to_date?

  requirements_to_unlock =
    if !checker.requirements_unlocked_or_can_be?
      if checker.can_update?(requirements_to_unlock: :none) then :none
      else :update_not_possible
      end
    elsif checker.can_update?(requirements_to_unlock: :own) then :own
    elsif checker.can_update?(requirements_to_unlock: :all) then :all
    else :update_not_possible
    end

  next if requirements_to_unlock == :update_not_possible


  updated_deps = checker.updated_dependencies(
    requirements_to_unlock: requirements_to_unlock
  )
  next if dep.name && @ignoreDependenciesHash.has_key?(dep.name) && @ignoreDependenciesHash[dep.name].include?(checker.latest_version.to_s) #New code

  #####################################
  # Generate updated dependency files #
  #####################################
  print "  - Updating #{dep.name} (from #{dep.version})…"
  updater = Dependabot::FileUpdaters.for_package_manager(package_manager).new(
    dependencies: updated_deps,
    dependency_files: files,
    credentials: credentials,
    options: options,
  )

  updated_files = updater.updated_dependency_files

  ########################################
  # Create a pull request for the update #
  ########################################
  assignee = (ENV["PULL_REQUESTS_ASSIGNEE"] || ENV["GITLAB_ASSIGNEE_ID"])&.to_i
  assignees = assignee ? [assignee] : assignee

  message = Dependabot::PullRequestCreator::MessageBuilder.new(
    source: source,
    dependencies: updated_deps,
    files: updated_files,
    credentials: credentials,
    commit_message_options: {},
    pr_message_header: nil,
    pr_message_footer: nil,
    vulnerabilities_fixed: {},
    github_redirection_service: "redirect.github.com"
  )

  branch_namer = Dependabot::PullRequestCreator::BranchNamer.new(
    dependencies: updated_deps,
    files: files,
    target_branch: source.branch,
    separator: "/",
    prefix: "dependabot"
  )

  labeler = Dependabot::PullRequestCreator::Labeler.new(
    source: source,
    custom_labels: nil,
    credentials: credentials,
    includes_security_fixes: false,
    dependencies: updated_deps,
    label_language: false,
    automerge_candidate: false
  )
  pr_creator = Dependabot::PullRequestCreator::Gitlab.new(
    source: source,
    branch_name: branch_namer.new_branch_name,
    base_commit: commit,
    credentials: credentials,
    files: updated_files,
    commit_message: message.commit_message,
    pr_description: message.pr_message,
    pr_name: message.pr_name,
    author_details: { name: "Dependabot", email: "no-reply@github.com" },
    labeler: labeler,
    approvers: nil,
    assignees: assignees,
    milestone: nil,
    target_project_id: nil
  )

  vulnerability_details = check_vulnerability(dep.name,dep.version)
  if dep.name && dep.version && vulnerability_details
    new_pr_message = <<~HEREDOC
      #### Affected versions
      #{vulnerability_details["VulnerableVersionRange"]}
      #### Severity
      #{vulnerability_details["Severity"]}

      ## Description
      #{vulnerability_details["Description"]}
      ### Reference links

    HEREDOC
    vulnerability_details["Links"].each do|vul|
      new_pr_message+= <<~HEREDOC
      [#{vul.url}](#{vul.url})

      HEREDOC
    end
    new_pr_message+=message.pr_message

    new_pr_name = "[Security update] : "+message.pr_name

    labeler = Dependabot::PullRequestCreator::Labeler.new(
      source: source,
      custom_labels: nil,
      credentials: credentials,
      includes_security_fixes: true,
      dependencies: updated_deps,
      label_language: false,
      automerge_candidate: false
    )
    pr_creator = Dependabot::PullRequestCreator::Gitlab.new(
      source: source,
      branch_name: branch_namer.new_branch_name,
      base_commit: commit,
      credentials: credentials,
      files: updated_files,
      commit_message: message.commit_message,
      pr_description: new_pr_message,
      pr_name: new_pr_name,
      author_details: { name: "Dependabot", email: "no-reply@github.com" },
      labeler: labeler,
      approvers: nil,
      assignees: assignees,
      milestone: nil,
      target_project_id: nil
    )
  end
  pull_request = pr_creator.create

  ENV["BRANCH_NAME"] = pr_creator.branch_name
  system(`node packageLockUpdater.js`)
  puts " submitted"

  next unless pull_request
  @ignoreDependenciesHash[dep.name] ||= []
  @ignoreDependenciesHash[dep.name] << (checker.latest_version.to_s)

  # Enable GitLab "merge when pipeline succeeds" feature.
  # Merge requests created and successfully tested will be merge automatically.
  if ENV["GITLAB_AUTO_MERGE"]
    g = Gitlab.client(
      endpoint: source.api_endpoint,
      private_token: ENV["GITLAB_ACCESS_TOKEN"]
    )
    g.accept_merge_request(
      source.repo,
      pull_request.iid,
      merge_when_pipeline_succeeds: true,
      should_remove_source_branch: true
    )
  end
end

puts "Done"
