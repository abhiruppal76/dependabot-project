const { spawn } = require('child_process');
const axios = require('axios');
require('dotenv').config();
const {getProjectIdFromProjectPath} = require('../../utils/getProjectId');
const {findPackageManagerFiles} = require('../../utils/findPackageManagerFiles');
const fs = require('fs');

const updateScript = async ()=>{

    const projectPath = process.env.PROJECT_PATH
    const gitlab_access_token = process.env.GITLAB_ACCESS_TOKEN;
    const projectId = await getProjectIdFromProjectPath(projectPath,gitlab_access_token);
    if(!projectId){
        return;
    }
    process.env.PROJECT_ID = projectId;
    const packageManagerFiles = await findPackageManagerFiles(projectId,process.env.GITLAB_ACCESS_TOKEN);
    packageManagerFiles.forEach(async packageManagerFile => {
        
        // setting the required environment variables
        const packageManager = 'npm_and_yarn';
        const directory = packageManagerFile;
        process.env.PACKAGE_MANAGER = packageManager;
        process.env.DIRECTORY_PATH = directory;
        console.log("Running for the filepath : ",directory);
       
        // executing the ruby wrapper script
        const rubyScriptPath = './wrapper.rb';
        const rubyProcess = spawn('ruby', [rubyScriptPath]);

        rubyProcess.stdout.on('data', (data) => {
            console.log(data.toString());
        });

        rubyProcess.stderr.on('data', (data) => {
            console.log(data.toString());
        });

        rubyProcess.on('close', (code) => {
            console.log(`Process exited with code ${code}`);
        });
    })
        
}

updateScript();